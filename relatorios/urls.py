from django.urls import include, path
from rest_framework import routers

from . import views

router = routers.DefaultRouter()
router.register(r'territorios', views.TerritorioAgregadoViewSet)

urlpatterns = [
    # path('',views.index,name='MainIndex'),

    path('', views.IndexView.as_view(), name="RelatoriosIndex"),
    path('territorios', views.TerritorioView.as_view(), name="TerritorioAgregadoIndex"),
    path('api/', include(router.urls)),
]

#
# urlpatterns += (
#     # urls for Organizacao
#     path('organizacao/', views.OrganizacaoListView.as_view(), name='organizacao_list'),
#     path('organizacao/create/', views.OrganizacaoCreateView.as_view(), name='organizacao_create'),
#     path('organizacao/detail/<int:pk>/', views.OrganizacaoDetailView.as_view(), name='organizacao_detail'),
#     path('organizacao/update/<int:pk>/', views.OrganizacaoUpdateView.as_view(), name='organizacao_update'),
#     path('organizacao/delete/<int:pk>/', views.OrganizacaoDeleteView.as_view(),
#          name='organizacao_delete'),
# )
