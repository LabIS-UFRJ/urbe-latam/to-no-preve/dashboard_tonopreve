from django.shortcuts import render, redirect
from django.views.generic import TemplateView, DeleteView, ListView, CreateView, DetailView, UpdateView
from rules.contrib.views import PermissionRequiredMixin

from relatorios.models import TotaisGerais, TotaisStatus, TotaisTipoComunidade, TotaisAreasDeUso, TotaisConflitos, \
    CadastrosUsuarias, CadastrosTerritorios, EstadosTerritorios, TerritorioAgregado

from rest_framework import routers, serializers, viewsets, permissions

from relatorios.serializers import TerritorioSerializer


class IndexView(TemplateView):
    template_name = "relatorios/index.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['totais_gerais'] = TotaisGerais.objects.first()
        context['totais_status'] = TotaisStatus.objects.all()
        context['totais_tipo_comunidade'] = TotaisTipoComunidade.objects.all()
        context['totais_areas_de_uso'] = TotaisAreasDeUso.objects.all()
        context['totais_conflitos'] = TotaisConflitos.objects.all()
        context['cadastros_usuarias'] = CadastrosUsuarias.objects.all()
        context['cadastros_territorios'] = CadastrosTerritorios.objects.all()
        context['estados_territorios'] = EstadosTerritorios.objects.all()
        return context

    def dispatch(self, request, *args, **kwargs):
        if self.request.user.is_anonymous:
            return redirect('MainIndex')

        return super(IndexView, self).dispatch(request, *args, **kwargs)


class TerritorioAgregadoViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = TerritorioAgregado.objects.all()
    serializer_class = TerritorioSerializer
    permission_classes = [permissions.IsAuthenticated]


class TerritorioView(PermissionRequiredMixin, TemplateView):
    template_name = "relatorios/territorios.html"
    permission_required = 'usuaria.administrar'


