from django import template
from django.template.defaultfilters import slugify

register = template.Library()


@register.inclusion_tag('relatorios/inclusiontags/grafico_donut.html')
def grafico_donut(*args, **kwargs):
    grafico_args = {}
    grafico_args['jsonify_queryset_opts'] = "id," + kwargs['label_field'] + ',' + kwargs['value_field']

    return {**kwargs, **grafico_args}


@register.inclusion_tag('relatorios/inclusiontags/grafico_plot.html')
def grafico_plot(*args, **kwargs):
    grafico_args = {}

    grafico_args['jsonify_queryset_opts'] = "id," + kwargs['time_field'] + ',' + kwargs['value_field']

    return {**kwargs, **grafico_args}


@register.inclusion_tag('relatorios/inclusiontags/grafico_mapa.html')
def grafico_mapa(*args, **kwargs):
    grafico_args = {}

    grafico_args['jsonify_queryset_opts'] = "id," + kwargs['territory_label'] + ',' + kwargs['territory_value']

    return {**kwargs, **grafico_args}