from django.contrib.gis.db import models

from main.models import TerritorioStatus, ModoDeVida, TipoProducao, Municipio


class TotaisGerais(models.Model):
    qtd_territorios = models.IntegerField()
    area_territorios = models.DecimalField(max_digits=12, decimal_places=9)
    qtd_familias = models.IntegerField()
    qtd_usuarios = models.IntegerField()
    qtd_areasdeuso = models.IntegerField()
    qtd_conflitos = models.IntegerField()

    class Meta:
        managed = False


class TotaisTipoComunidade(models.Model):
    nome = models.CharField(max_length=255)
    qtd_territorios = models.IntegerField()
    qtd_familias = models.IntegerField()

    class Meta:
        managed = False


class TotaisAreasDeUso(models.Model):
    nome = models.CharField(max_length=255)
    qtd_areasdeuso = models.IntegerField()

    class Meta:
        managed = False


class TotaisConflitos(models.Model):
    nome = models.CharField(max_length=255)
    qtd_conflitos = models.IntegerField()

    class Meta:
        managed = False


class TotaisStatus(models.Model):
    nome = models.CharField(max_length=255)
    qtd_territorios = models.IntegerField()

    class Meta:
        managed = False


class CadastrosUsuarias(models.Model):
    dia = models.IntegerField()
    qtd_usuarias = models.IntegerField()

    class Meta:
        managed = False


class CadastrosTerritorios(models.Model):
    dia = models.IntegerField()
    qtd_territorios = models.IntegerField()

    class Meta:
        managed = False


class EstadosTerritorios(models.Model):
    uf = models.CharField(max_length=2)
    nome = models.CharField(max_length=50)
    qtd_territorios = models.IntegerField()

    class Meta:
        managed = False


class TerritorioAgregado(models.Model):
    nome = models.CharField(max_length=255, blank=True, null=True)
    qtde_familias = models.IntegerField(blank=True, null=True)
    ano_fundacao = models.IntegerField(blank=True, null=True)
    status = models.ForeignKey(TerritorioStatus, models.DO_NOTHING, related_name='territorios_a', blank=True, null=True)
    modo_de_vida = models.ForeignKey(ModoDeVida, models.DO_NOTHING, related_name='territorios_a', blank=True, null=True)
    tipo_producao = models.ForeignKey(TipoProducao, models.DO_NOTHING, related_name='territorios_a', blank=True, null=True)
    criacao = models.DateTimeField(auto_now_add=True, editable=False)
    ultima_alteracao = models.DateTimeField(auto_now=True, editable=False)
    municipio_referencia = models.ForeignKey(Municipio, models.DO_NOTHING, related_name='territorios_a')
    publico = models.BooleanField(default=False, blank=True, null=True)
    anexo_ata = models.ForeignKey('main.Anexo', models.DO_NOTHING, related_name='territorios_a', blank=True, null=True)
    municipio_nome = models.CharField(max_length=150, blank=True, null=True)
    estado_uf = models.CharField(max_length=2, blank=True, null=True)
    estado_nome = models.CharField(max_length=50, blank=True, null=True)
    status_nome = models.CharField(max_length=255, blank=True, null=True)
    tipos_comunidade = models.CharField(max_length=4096, blank=True, null=True)
    tipos_areas_de_uso = models.CharField(max_length=4096, blank=True, null=True)
    tipos_conflitos = models.CharField(max_length=4096, blank=True, null=True)

    class Meta:
        managed = False
