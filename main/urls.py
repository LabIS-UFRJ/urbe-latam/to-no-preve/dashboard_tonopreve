from django.conf import settings
from django.conf.urls.static import static
from django.urls import path
from django.views.generic import RedirectView

from . import views

urlpatterns = [
    # path('',views.index,name='MainIndex'),

    path('', views.IndexView.as_view(), name="MainIndex"),
    path('anexos/', views.AnexoView.as_view(), name='anexos_view'),
    path('termosdeuso', views.TermosDeUsoView.as_view(), name="termosdeuso_view")

]

urlpatterns += (
    # urls for Organizacao
    path('organizacao/', views.OrganizacaoListView.as_view(), name='organizacao_list'),
    path('organizacao/create/', views.OrganizacaoCreateView.as_view(), name='organizacao_create'),
    path('organizacao/detail/<int:pk>/', views.OrganizacaoDetailView.as_view(), name='organizacao_detail'),
    path('organizacao/update/<int:pk>/', views.OrganizacaoUpdateView.as_view(), name='organizacao_update'),
    path('organizacao/delete/<int:pk>/', views.OrganizacaoDeleteView.as_view(),
         name='organizacao_delete'),
)

urlpatterns += (
    # urls for OrganizacaoUsuaria
    path('organizacaousuaria/update', views.OrganizacaoUsuariaUpdateView.as_view(), name='organizacaousuaria_update'),
    path('organizacaousuaria/delete/<int:pk>', views.OrganizacaoUsuariaDeleteView.as_view(),
         name='organizacaousuaria_delete')
)

urlpatterns += (
    path('organizacao/<int:organizacao_id>/create_usuaria/', views.UsuariaCreateView.as_view(), name='org_usuaria_create'),

    path('usuarias/', views.UsuariaListView.as_view(), name='usuaria_list'),
    path('usuarias/detail/<int:pk>/', views.UsuariaDetailView.as_view(), name='usuaria_detail'),
    path('usuarias/update/<int:pk>/', views.UsuariaUpdateView.as_view(), name='usuaria_update'),
)

urlpatterns += (
    # urls for Territorio
    path('territorio/', views.TerritorioListView.as_view(), name='territorio_list'),
    path('territorio/create/', views.TerritorioCreateView.as_view(), name='territorio_create'),
    path('territorio/detail/<int:pk>/', views.TerritorioDetailView.as_view(), name='territorio_detail'),
    path('territorio/update/<int:pk>/', views.TerritorioUpdateView.as_view(), name='territorio_update'),
    path('territorio/delete/<int:pk>/', views.TerritorioDeleteView.as_view(),
         name='territorio_delete'),
    path('territorio/mensagens/<int:territorio_id>/', views.MensagemCreateView.as_view(), name='territorio_mensagens')

)

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
