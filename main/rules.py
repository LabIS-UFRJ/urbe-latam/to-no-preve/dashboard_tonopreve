import rules

from main.models import OrganizacaoUsuaria


@rules.predicate
def eh_superusuario(usuaria):
    return usuaria.is_superuser


eh_admin_geral = rules.is_group_member('admin')


@rules.predicate
def eh_anonimo(usuaria):
    return usuaria.is_anonymous


@rules.predicate
def eh_admin_da_organizacao(usuaria, organizacao):
    try:
        ou = OrganizacaoUsuaria.objects.get(usuaria_id=usuaria.id, organizacao_id=organizacao.id, is_admin=True)
        return True
    except OrganizacaoUsuaria.DoesNotExist:
        return False


@rules.predicate
def eh_admin_de_alguma_organizacao(usuaria):
    ous = OrganizacaoUsuaria.objects.filter(usuaria_id=usuaria.id, is_admin=True)
    return ous.count() > 0


@rules.predicate
def eh_integrante_de_alguma_organizacao(usuaria):
    ous = OrganizacaoUsuaria.objects.filter(usuaria_id=usuaria.id)
    return ous.count() > 0


@rules.predicate
def eh_integrante_da_organizacao(usuaria, organizacao):
    try:
        ou = OrganizacaoUsuaria.objects.get(usuaria_id=usuaria.id, organizacao_id=organizacao.id)
        return True
    except OrganizacaoUsuaria.DoesNotExist:
        return False


rules.add_perm('organizacao.ver_todos', eh_admin_geral)
rules.add_perm('organizacao.cadastrar', eh_superusuario | eh_admin_geral)
rules.add_perm('organizacao.alterar', eh_superusuario | eh_admin_geral | eh_admin_da_organizacao)
rules.add_perm('organizacao.visualizar', eh_superusuario | eh_admin_geral | eh_integrante_da_organizacao)
rules.add_perm('organizacao.apagar', eh_superusuario | eh_admin_geral | eh_admin_da_organizacao)

rules.add_perm('territorio.ver_todos', eh_admin_geral)
rules.add_perm('territorio.cadastrar', eh_superusuario | eh_admin_geral)
rules.add_perm('territorio.alterar', eh_superusuario | eh_admin_geral | eh_admin_da_organizacao)
rules.add_perm('territorio.visualizar', eh_superusuario | eh_admin_geral | eh_integrante_da_organizacao)
rules.add_perm('territorio.apagar', eh_superusuario | eh_admin_geral | eh_admin_da_organizacao)


rules.add_perm('permissao_padrao', ~eh_anonimo)

rules.add_perm('usuaria.administrar', eh_superusuario | eh_admin_geral)
