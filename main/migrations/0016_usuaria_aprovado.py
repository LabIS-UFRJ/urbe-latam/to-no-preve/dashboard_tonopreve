# Generated by Django 3.0.3 on 2020-05-07 13:00

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0015_auto_20200506_1237'),
    ]

    operations = [
        migrations.AddField(
            model_name='usuaria',
            name='aprovado',
            field=models.BooleanField(default=False),
        ),
    ]
