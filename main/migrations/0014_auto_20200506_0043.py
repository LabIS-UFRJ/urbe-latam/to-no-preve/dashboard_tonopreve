# Generated by Django 3.0.3 on 2020-05-06 00:43

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0013_territorio_status_values'),
    ]

    operations = [
        migrations.AlterField(
            model_name='territorio',
            name='status',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='main.TerritorioStatus'),
        ),
    ]
