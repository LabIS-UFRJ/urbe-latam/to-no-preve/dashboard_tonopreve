from django.contrib.auth.models import Group
from django.db import IntegrityError, transaction
from django.db.models import ProtectedError
from django.http import JsonResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, redirect
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.utils.timezone import now
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import TemplateView, DeleteView, ListView, CreateView, DetailView, UpdateView
from rest_framework import status
from rest_framework.parsers import MultiPartParser, FormParser
from rest_framework.response import Response
from rest_framework.views import APIView
from rules.contrib.views import PermissionRequiredMixin

from main.forms import OrganizacaoForm, OrganizacaoUsuariaForm, UsuariaCreationForm, UsuariaChangeForm, \
    MensagemCreationForm, TerritorioForm, TerritorioDetailForm
from main.mixins import AjaxableResponseMixin
from main.models import Organizacao, Usuaria, OrganizacaoUsuaria, Mensagem, Territorio
from .models import Anexo
from .serializers import AnexoSerializer


class IndexView(TemplateView):
    template_name = "main/index.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context

    def dispatch(self, request, *args, **kwargs):
        if not self.request.user.is_anonymous:
            return redirect('RelatoriosIndex')

        return super(IndexView, self).dispatch(request, *args, **kwargs)


class BaseDeleteView(DeleteView):

    def delete(self, request, *args, **kwargs):
        status = 200

        try:
            self.get_object().delete()
            payload = {'delete': 'ok'}
        except (ProtectedError, IntegrityError) as e:
            payload = {'error': "Erro ao apagar", 'message': "{0}".format(e)}
            status = 405  # Method not allowed

        return JsonResponse(payload, status=status)


class OrganizacaoListView(ListView):
    model = Organizacao

    def get_queryset(self):
        if self.request.user.has_perm('organizacao.ver_todos'):
            return Organizacao.objects.all()
        elif self.request.user.is_anonymous:
            return Organizacao.objects.none()
        else:
            return Organizacao.objects.filter(usuarias=self.request.user)


class OrganizacaoCreateView(PermissionRequiredMixin, CreateView):
    model = Organizacao
    form_class = OrganizacaoForm
    permission_required = 'organizacao.cadastrar'
    titulo = "Nova Organização"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['titulo'] = self.titulo
        context['modulo'] = 'main'
        return context


class OrganizacaoDetailView(PermissionRequiredMixin, DetailView):
    model = Organizacao
    permission_required = 'organizacao.visualizar'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        usuarias_do_sistema = Usuaria.objects.filter(is_superuser=False).difference(self.object.usuarias.all())

        context['usuarias_do_sistema'] = usuarias_do_sistema
        context['modulo'] = 'main'
        context['org_usuarias'] = self.get_object().org_usuarias.order_by('usuaria__username')
        return context


class OrganizacaoUpdateView(PermissionRequiredMixin, UpdateView):
    model = Organizacao
    form_class = OrganizacaoForm
    permission_required = 'organizacao.alterar'
    titulo = "Editar Organização"

    def get_form_kwargs(self):
        kwargs = super(OrganizacaoUpdateView, self).get_form_kwargs()
        kwargs.update({'can_delete': self.request.user.has_perm('organizacao.apagar', self.get_object())})
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['titulo'] = self.titulo
        context['modulo'] = 'main'
        context['org_usuarias'] = self.get_object().org_usuarias.order_by('usuaria__username')
        return context


class OrganizacaoDeleteView(PermissionRequiredMixin, BaseDeleteView):
    model = Organizacao
    permission_required = 'organizacao.apagar'

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(OrganizacaoDeleteView, self).dispatch(*args, **kwargs)


class OrganizacaoUsuariaListView(ListView):
    model = OrganizacaoUsuaria


class OrganizacaoUsuariaUpdateView(PermissionRequiredMixin, UpdateView):
    model = OrganizacaoUsuaria
    form_class = OrganizacaoUsuariaForm
    permission_required = 'organizacao.alterar'

    def get_permission_object(self):
        return self.get_object().organizacao

    def dispatch(self, *args, **kwargs):
        return super(OrganizacaoUsuariaUpdateView, self).dispatch(*args, **kwargs)

    def get_object(self, queryset=None):
        # get the existing object or created a new one
        data = self.request.POST

        with transaction.atomic():
            obj, created = OrganizacaoUsuaria.objects.get_or_create(usuaria_id=data.get('usuaria', None),
                                                                    organizacao_id=data.get('organizacao', None))

            is_admin = data.get('is_admin', False)
            if is_admin == 'true':
                obj.is_admin = True

            is_mapeador = data.get('is_mapeador', False)
            if is_mapeador == 'true':
                obj.is_mapeador = True

            is_revisor = data.get('is_revisor', False)
            if is_revisor == 'true':
                obj.is_revisor = True

            obj.save()
            return obj

    def get_success_url(self):
        # get the existing object or created a new one
        data = self.request.POST

        return reverse('organizacao_detail', kwargs={'pk': int(data.get('organizacao', None))})

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['titulo'] = self.titulo
        context['modulo'] = 'main'
        return context


class OrganizacaoUsuariaDeleteView(PermissionRequiredMixin, AjaxableResponseMixin, DeleteView):
    model = OrganizacaoUsuaria
    organizacao = None
    permission_required = 'organizacao.alterar'

    def get_permission_object(self):
        return self.get_object().organizacao

    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        self.organizacao = self.get_object().organizacao
        self.object.delete()

        data = {
            'status': 'success',
            'message': "Apagado com sucesso!",
            'redirect': self.get_success_url()
        }
        return JsonResponse(data)

    def get_success_url(self):
        return reverse('organizacao_detail', kwargs={'pk': self.organizacao.pk})


class UsuariaCreateView(PermissionRequiredMixin, CreateView):
    model = Usuaria
    form_class = UsuariaCreationForm
    permission_required = 'organizacao.alterar'
    titulo = "Cadastro de nova(o) usuária(o)"

    def get_permission_object(self):
        return Organizacao.objects.get(pk=self.kwargs['organizacao_id'])

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['organizacao'] = Organizacao.objects.get(pk=self.kwargs['organizacao_id'])
        context['modulo'] = 'main'
        context['titulo'] = self.titulo
        return context

    def form_valid(self, form):
        obj = form.save(commit=False)
        obj.save()

        organizacao = Organizacao.objects.get(pk=self.kwargs['organizacao_id'])

        OrganizacaoUsuaria.objects.create(organizacao_id=organizacao.id,
                                          usuaria_id=obj.id,
                                          is_admin=form.cleaned_data['is_organization_admin'],
                                          is_mapeador=form.cleaned_data['is_organization_mapeador'],
                                          is_revisor=form.cleaned_data['is_organization_revisor']
                                          )

        gr_usuarios = Group.objects.get(name='usuarios')
        gr_usuarios.user_set.add(obj)

        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse('organizacao_detail', kwargs={'pk': self.kwargs['organizacao_id']})


class UsuariaUpdateView(PermissionRequiredMixin, UpdateView):
    model = Usuaria
    form_class = UsuariaChangeForm
    permission_required = 'usuaria.administrar'
    titulo = "Editar dados de usuária(o)"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['modulo'] = 'main'
        context['titulo'] = self.titulo
        return context

    def get_success_url(self):
        return reverse('usuaria_list')


class UsuariaListView(PermissionRequiredMixin, ListView):
    model = Usuaria
    permission_required = 'usuaria.administrar'

    def get_queryset(self):
        # TODO condição - autorização
        return Usuaria.objects.all()


class UsuariaDetailView(PermissionRequiredMixin, DetailView):
    model = Usuaria
    permission_required = 'usuaria.administrar'


class MensagemListView(PermissionRequiredMixin, ListView):
    model = Mensagem
    permission_required = 'usuaria.administrar'
    template_name = 'main/territorio_mensagens_list.html'

    def get_queryset(self):
        territorio = get_object_or_404(Territorio, id=self.kwargs['territorio_id'])
        return Mensagem.objects.filter(territorio=territorio).order_by('data_envio')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['usuaria'] = get_object_or_404(Usuaria, id=self.kwargs['usuaria_id'])
        return context


class MensagemCreateView(PermissionRequiredMixin, CreateView):
    model = Mensagem
    form_class = MensagemCreationForm
    permission_required = 'usuaria.administrar'
    titulo = "Enviar Mensagem"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        territorio = get_object_or_404(Territorio, id=self.kwargs['territorio_id'])
        context['territorio'] = territorio
        context['titulo'] = "Mensagens do território " + str(territorio)
        context['mensagens_antigas'] = self.mensagens_antigas
        return context

    def form_valid(self, form):
        obj = form.save(commit=False)
        # obj.save()

        territorio = get_object_or_404(Territorio, id=self.kwargs['territorio_id'])

        mensagem = territorio.send_push_message(form.cleaned_data['texto'], self.request.user)

        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse('territorio_mensagens', kwargs={'territorio_id': self.kwargs['territorio_id']})

    @property
    def mensagens_antigas(self):
        territorio = get_object_or_404(Territorio, id=self.kwargs['territorio_id'])
        return Mensagem.objects.filter(territorio=territorio).order_by('data_envio').all()


class AnexoView(APIView):
    parser_classes = (MultiPartParser, FormParser)

    def get(self, request, *args, **kwargs):
        anexos = Anexo.objects.all()
        serializer = AnexoSerializer(anexos, many=True)
        return Response(serializer.data)

    def post(self, request, *args, **kwargs):
        anexo = None
        try:
            anexo = Anexo.objects.get(id=request.data['id'])
        except:
            pass

        anexos_serializer = AnexoSerializer(data=request.data, instance=anexo)
        if anexos_serializer.is_valid():
            anexo = anexos_serializer.save()
            # se o tipo de anexo for video, cria o thumbnail na mão, pois dá um erro no automático

            return Response(anexos_serializer.data, status=status.HTTP_201_CREATED)
        else:
            print('error', anexos_serializer.errors)
            return Response(anexos_serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class TerritorioListView(ListView):
    model = Territorio

    def get_queryset(self):
        if self.request.user.has_perm('territorio.ver_todos'):
            return Territorio.objects.all().order_by('-ultima_alteracao')
        elif self.request.user.is_anonymous:
            return Territorio.objects.none()
        else:
            return Territorio.objects.filter(usuarias=self.request.user).order_by('-ultima_alteracao')


class TerritorioCreateView(PermissionRequiredMixin, CreateView):
    model = Territorio
    form_class = TerritorioForm
    permission_required = 'territorio.cadastrar'
    titulo = "Novo Território"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['titulo'] = self.titulo
        context['modulo'] = 'main'
        return context


class TerritorioDetailView(PermissionRequiredMixin, UpdateView):
    model = Territorio
    permission_required = 'territorio.visualizar'
    form_class = TerritorioDetailForm
    template_name = "main/territorio_detail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['modulo'] = 'main'

        try:
            position_initial = self.object.poligono.point_on_surface.tuple
            context['position_initial'] = [position_initial[1], position_initial[0]]
        except:
            context['position_initial'] = []

        try:
            context['poligonos'] = [[[y, x]
                                     for x, y in poligono]
                                    for poligono in self.object.poligono.coords[0]]
        except:
            context['poligonos'] = []

        try:
            context['conflitos'] = [
                {'lat': conflito.posicao.tuple[1], 'lng': conflito.posicao.tuple[0], 'nome': conflito.nome}
                for conflito in self.object.conflitos.all()]
        except:
            context['conflitos'] = []

        try:
            context['areas_uso'] = [{'lng': area.posicao.tuple[1], 'lat': area.posicao.tuple[0], 'nome': area.nome}
                                    for area in self.object.areas_de_uso.all()]
        except:
            context['areas_uso'] = []

        return context

    def form_valid(self, form):
        super().form_valid(form)

        territorio = self.object

        texto = ''
        if 'status' in form.changed_data:
            texto += f"Territorio {territorio}: O Status foi alterado para {territorio.status} \n"
        mensagem = form.cleaned_data['mensagem']
        if mensagem:
            texto += f"Com a seguinte mensagem: {mensagem}"

        if texto:
            send_mensagem = territorio.send_push_message(texto, self.request.user)

        return super().form_valid(form)


class TerritorioUpdateView(PermissionRequiredMixin, UpdateView):
    model = Territorio
    form_class = TerritorioForm
    permission_required = 'territorio.alterar'
    titulo = "Editar Território"

    def get_form_kwargs(self):
        kwargs = super(TerritorioUpdateView, self).get_form_kwargs()
        kwargs.update({'can_delete': self.request.user.has_perm('territorio.apagar', self.get_object())})
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['titulo'] = self.titulo
        context['modulo'] = 'main'
        context['org_usuarias'] = self.get_object().org_usuarias.order_by('usuaria__username')
        return context


class TerritorioDeleteView(PermissionRequiredMixin, BaseDeleteView):
    model = Territorio
    permission_required = 'territorio.apagar'

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(TerritorioDeleteView, self).dispatch(*args, **kwargs)


class TermosDeUsoView(TemplateView):
    template_name = "main/termosdeuso.html"
