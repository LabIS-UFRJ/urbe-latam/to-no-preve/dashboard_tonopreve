from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import UserChangeForm

from main.models import Usuaria, Organizacao, Estado, ModoDeVida, Municipio, TerritorioStatus, Territorio, \
    TipoAreaDeUso, TipoComunidade, TipoConflito, TipoProducao, TipoAnexo, TerritorioUsuaria, OrganizacaoUsuaria, \
    Conflito, AreaDeUso, Anexo, Mensagem, Device, MensagemDevice


# Register your models here.

class UsuariaChangeForm(UserChangeForm):
    class Meta(UserChangeForm.Meta):
        model = Usuaria


class UsuariaAdmin(UserAdmin):
    form = UsuariaChangeForm

    fieldsets = UserAdmin.fieldsets + (
        (None, {'fields': ('telefone', 'codigo_uso_celular',)}),
    )


admin.site.register(Usuaria, UsuariaAdmin)
admin.site.register(Organizacao)
admin.site.register(Estado)
admin.site.register(ModoDeVida)
admin.site.register(Municipio)
admin.site.register(Territorio)
admin.site.register(TerritorioStatus)
admin.site.register(TipoAreaDeUso)
admin.site.register(TipoComunidade)
admin.site.register(TipoConflito)
admin.site.register(TipoProducao)
admin.site.register(TipoAnexo)
admin.site.register(TerritorioUsuaria)
admin.site.register(OrganizacaoUsuaria)
admin.site.register(Conflito)
admin.site.register(AreaDeUso)
admin.site.register(Anexo)
admin.site.register(Mensagem)
admin.site.register(Device)
admin.site.register(MensagemDevice)
