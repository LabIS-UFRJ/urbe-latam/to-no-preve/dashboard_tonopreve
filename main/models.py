import magic
import os
from django.conf import settings
from django.contrib.auth.models import AbstractUser
from django.contrib.gis.db import models
from django.db import IntegrityError
from django.db.models import Q
from django.urls import reverse
from django.contrib.postgres.fields import JSONField
from django.utils import timezone
from django.utils.functional import cached_property

from django_archive_mixin.mixins import ArchiveMixin
import re
from random import randint

from imagekit import ImageSpec
from imagekit.models import ImageSpecField
from pilkit.processors import ResizeToFill, Transpose

from exponent_server_sdk import DeviceNotRegisteredError
from exponent_server_sdk import PushClient
from exponent_server_sdk import PushMessage
from exponent_server_sdk import PushResponseError
from exponent_server_sdk import PushServerError
from requests.exceptions import ConnectionError
from requests.exceptions import HTTPError


class BaseModel(ArchiveMixin, models.Model):
    class Meta:
        abstract = True


class Device(models.Model):
    usuaria = models.ForeignKey("Usuaria", models.PROTECT, related_name="devices")
    push_token = models.CharField(max_length=120)
    criacao = models.DateTimeField(auto_now_add=True, editable=False)
    ultima_alteracao = models.DateTimeField(auto_now=True, editable=False)
    active = models.BooleanField(default=True)


class Usuaria(AbstractUser):
    telefone = models.CharField(max_length=25, blank=True, null=True)
    codigo_uso_celular = models.CharField(max_length=8, blank=True, null=True)
    is_admin = models.BooleanField(blank=True, null=True)
    is_reviewer = models.BooleanField(blank=True, null=True)
    aprovado = models.BooleanField(blank=False, null=False, default=False)
    cpf = models.CharField(max_length=15, blank=True, null=True)

    __original_codigo_uso_celular = None

    def __init__(self, *args, **kwargs):
        super(Usuaria, self).__init__(*args, **kwargs)
        self.__original_codigo_uso_celular = self.codigo_uso_celular

    territorios = models.ManyToManyField(
        'Territorio',
        through='TerritorioUsuaria',
        related_name='usuarias'
    )

    def current_territorio(self, id_territorio=None):
        if id_territorio:
            return self.territorios.filter(pk=id_territorio).first()
        else:
            return self.territorios.order_by('-ultima_alteracao').first()

    def __str__(self):
        return self.get_full_name() if self.get_full_name() != "" else self.username

    def get_displayname(self):
        return self.first_name if self.first_name != '' else self.username

    def get_absolute_url(self):
        return reverse('usuaria_detail', args=(self.pk,))

    def get_update_url(self):
        return reverse('usuaria_update', args=(self.pk,))

    def set_full_name(self, full_name):
        full_name_splitted = re.sub(" +", " ", full_name).strip().split(" ", 1)
        if len(full_name_splitted) == 1:
            self.first_name = full_name_splitted[0]
            self.last_name = ''
        else:
            self.first_name, self.last_name = full_name_splitted

    @classmethod
    def gera_senha_aleatoria(self):
        """
        :return: uma string com 7 dígitos
        """
        return "{input:07d}".format(input=randint(0, 9999999))

    # guarda um objeto Device associado a esta usuária com o token fornecido.
    def create_or_update_device(self, device_expo_id):
        if not self.id:
            raise IntegrityError("Usuaria is not saved yet")

        if device_expo_id is None:  # App não atualizado
            return

        changed = False
        try:
            device = Device.objects.get(push_token=device_expo_id)
        except Device.DoesNotExist:
            device = Device()
            device.push_token = device_expo_id
            changed = True

        if device.usuaria_id != self.id:
            device.usuaria_id = self.id
            changed = True

        if changed:
            device.active = True
            device.save()

    def save(self, force_insert=False, force_update=False, *args, **kwargs):
        if self.codigo_uso_celular != self.__original_codigo_uso_celular:
            self.set_password(self.codigo_uso_celular)
        super(Usuaria, self).save(force_insert, force_update, *args, **kwargs)
        self.__original_codigo_uso_celular = self.codigo_uso_celular

    class Meta:
        db_table = 'usuaria'
        ordering = ('username',)


# ESTADO / MUNICIPIO
class Estado(models.Model):
    uf = models.CharField(max_length=2, blank=True, null=True)
    nome = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        pass

    def __str__(self):
        return self.nome


class Municipio(models.Model):
    estado = models.ForeignKey(Estado, models.PROTECT)
    nome = models.CharField(max_length=150, blank=True, null=True)
    latitude = models.DecimalField(blank=True, null=True, max_digits=7, decimal_places=5)
    longitude = models.DecimalField(blank=True, null=True, max_digits=8, decimal_places=5)
    capital = models.BooleanField(blank=True, null=True)

    class Meta:
        pass

    def __str__(self):
        return self.nome


# TERRITÓRIO
class ModoDeVida(BaseModel):
    nome = models.CharField(max_length=60, blank=True, null=True)
    criacao = models.DateTimeField(auto_now_add=True, editable=False)
    ultima_alteracao = models.DateTimeField(auto_now=True, editable=False)

    def __str__(self):
        return self.nome

    class Meta:
        pass


class TipoComunidade(BaseModel):
    nome = models.CharField(max_length=60, blank=True, null=True)
    descricao = models.TextField(blank=True, null=True)

    class Meta:
        pass

    def __str__(self):
        return self.nome


class TipoProducao(BaseModel):
    nome = models.CharField(max_length=60, blank=True, null=True)

    class Meta:
        pass

    def __str__(self):
        return self.nome


class TerritorioStatus(BaseModel):
    nome = models.CharField(max_length=255, blank=True, null=True)
    mensagem = models.TextField(blank=True, null=True)

    def __str__(self):
        return str(self.id) + ' - ' + self.nome


class Territorio(BaseModel):
    nome = models.CharField(max_length=255, blank=True, null=True)
    poligono = models.MultiPolygonField(blank=True, null=True)
    ano_fundacao = models.IntegerField(blank=True, null=True)
    qtde_familias = models.IntegerField(blank=True, null=True)
    status = models.ForeignKey(TerritorioStatus, models.PROTECT, blank=True, null=True)
    modo_de_vida = models.ForeignKey(ModoDeVida, models.PROTECT, blank=True, null=True)
    tipo_producao = models.ForeignKey(TipoProducao, models.PROTECT, blank=True, null=True)
    municipio_referencia = models.ForeignKey(Municipio, models.PROTECT)
    criacao = models.DateTimeField(auto_now_add=True, editable=False)
    ultima_alteracao = models.DateTimeField(auto_now=True, editable=False)
    publico = models.BooleanField(default=False, blank=True, null=True)
    anexo_ata = models.ForeignKey('Anexo', models.CASCADE, related_name='territorios', blank=True, null=True)

    tipos_comunidade = models.ManyToManyField(
        TipoComunidade,
        related_name='territorios'
    )

    def __str__(self):
        return self.nome

    def get_messages_url(self):
        return reverse('territorio_mensagens', args=(self.pk,))

    def get_absolute_url(self):
        return reverse('territorio_detail', args=(self.pk,))

    def send_push_message(self, message, remetente=None, extra={}):
        """
        :type message: str
        :type remetente: Usuaria
        :type extra: dict, optional
        """
        devices = Device.objects.filter(usuaria__in=self.usuarias.all(), active=True).all()

        if len(devices) > 0:
            dataEnvio = timezone.now()
            extra['texto'] = message
            extra['dataEnvio'] = str(dataEnvio)
            extra['originIsDevice'] = False
            extra['territorioId'] = self.id
            # cria objeto 'Mensagem'
            mensagem = Mensagem(
                remetente=remetente,
                territorio=self,
                texto=extra['texto'],
                extra=extra,
                data_envio=dataEnvio,
                origin_is_device=extra['originIsDevice']
            )

            mensagem.save()
            mensagem.extra['id'] = mensagem.id
            mensagem.save()

            mds = mensagem.create_mensagem_device_objects()

            push_messages = []
            for device in devices:
                push_messages.append(PushMessage(to=device.push_token,
                                                 body=message,
                                                 data=extra))

            try:
                responses = PushClient().publish_multiple(push_messages)
            except PushServerError as exc:

                print(exc)
                # Encountered some likely formatting/validation error.
                for md in mds:
                    md.error_data = exc.errors
                    md.status = 3
                    md.save()
                raise
            except (ConnectionError, HTTPError) as exc:
                print(exc)
                # Encountered some Connection or HTTP error - retry a few times in
                # case it is transient.
                for md in mds:
                    md.error_data = exc.errors
                    md.status = 4
                    md.save()
                """
                rollbar.report_exc_info(
                    extra_data={'token': token, 'message': message, 'extra': extra})

                raise self.retry(exc=exc)
                """

            for response in responses:
                current_md = None
                for md in mds:
                    if md.device.push_token == response.push_message.to:
                        current_md = md
                try:
                    # We got a response back, but we don't know whether it's an error yet.
                    # This call raises errors so we can handle them with normal exception
                    # flows.
                    response.validate_response()
                    current_md.status = 1
                    current_md.save()
                    print("sent!")
                except DeviceNotRegisteredError as exc:
                    print(exc)
                    # Mark the device token as inactive
                    Device.objects.filter(push_token=response.push_message.to).update(active=False)
                    current_md.status = 5
                    current_md.save()
                except PushResponseError as exc:
                    print(exc)
                    # Encountered some other per-notification error.
                    current_md.status = 6
                    current_md.error_data = exc.push_response._asdict()
                    current_md.save()
                    """
                    rollbar.report_exc_info(
                        extra_data={
                            'token': token,
                            'message': message,
                            'extra': extra,
                            'push_response': exc.push_response._asdict(),
                        })
                    raise self.retry(exc=exc)
                    """
            return mensagem

    @property
    def mensagens(self):
        """
        lista de mensagens do território
        :return:
        """
        return Mensagem.objects.filter(territorio_id=self.id)

    class Meta:
        pass


class TerritorioUsuaria(models.Model):
    territorio = models.ForeignKey(Territorio, models.CASCADE)
    usuaria = models.ForeignKey(settings.AUTH_USER_MODEL, models.CASCADE)
    is_editor = models.BooleanField(blank=True, null=True)

    class Meta:
        pass

    def __str__(self):
        return str(self.territorio) + ' <-> ' + str(self.usuaria)


# ANEXOS
class TipoAnexo(BaseModel):
    nome = models.CharField(max_length=60, blank=True, null=True)
    mae = models.ForeignKey('self', models.PROTECT)

    class Meta:
        pass


class Anexo(BaseModel):
    nome = models.CharField(max_length=250)
    descricao = models.TextField(null=True, blank=True)
    mimetype = models.CharField(max_length=80, blank=True, null=True)
    file_size = models.IntegerField(blank=True, null=True)
    arquivo = models.FileField(upload_to='anexos/%Y/%m/%d/')
    tipo_anexo = models.ForeignKey(TipoAnexo, models.PROTECT, blank=True, null=True)
    territorio = models.ForeignKey(Territorio, models.CASCADE, related_name='anexos', blank=True, null=True)
    criacao = models.DateTimeField(auto_now_add=True, editable=False)
    ultima_alteracao = models.DateTimeField(auto_now=True, editable=False)

    thumb = models.ImageField(null=True, blank=True, upload_to='anexos/thumbs/%Y/%m/%d/')

    def __str__(self):
        return self.nome

    class Meta:
        pass

    class Thumbnail(ImageSpec):
        processors = [Transpose(), ResizeToFill(1440, 720)]
        format = 'JPEG'
        options = {'quality': 60}

    def save(self, *args, **kwargs):
        redo_thumb = kwargs.pop('redo_thumb', False)
        super(Anexo, self).save(*args, **kwargs)
        if self.arquivo is None:
            if self.mimetype is not None:
                self.file_size = None
                self.mimetype = None
                self.thumb = None
                super(Anexo, self).save()
        else:
            mime = magic.from_file(self.arquivo.path, mime=True)
            size = os.path.getsize(self.arquivo.path)
            do_save = False

            if self.mimetype != mime:
                self.mimetype = mime
                do_save = True

            if self.file_size != size:
                self.file_size = size
                do_save = True

            if do_save or redo_thumb:
                # generates thumbnail when mime is image
                if mime.startswith("image") and self.arquivo.name:
                    with open(self.arquivo.path, "rb") as source_file:
                        image_generator = self.Thumbnail(source=source_file)
                        thumb_file = image_generator.generate()
                        thumb_path = os.path.basename(self.arquivo.name)
                        self.thumb.save(thumb_path, thumb_file)
                else:
                    self.thumb = None

                super(Anexo, self).save()


# ÁREA DE USO
class TipoAreaDeUso(BaseModel):
    nome = models.CharField(max_length=255, blank=True, null=True)
    descricao = models.TextField(blank=True, null=True)

    class Meta:
        pass


class AreaDeUso(BaseModel):
    nome = models.CharField(max_length=120, blank=True, null=True)
    posicao = models.PointField(blank=True, null=True)
    descricao = models.TextField(blank=True, null=True)
    territorio = models.ForeignKey(Territorio, models.CASCADE, related_name="areas_de_uso")
    tipo_area_de_uso = models.ForeignKey(TipoAreaDeUso, models.PROTECT)
    criacao = models.DateTimeField(auto_now_add=True, editable=False)
    ultima_alteracao = models.DateTimeField(auto_now=True, editable=False)

    class Meta:
        pass


# CONFLITO
class TipoConflito(BaseModel):
    nome = models.CharField(max_length=255, blank=True, null=True)
    descricao = models.TextField(blank=True, null=True)

    class Meta:
        pass


class Conflito(BaseModel):
    nome = models.CharField(max_length=120, blank=True, null=True)
    posicao = models.PointField(blank=True, null=True)
    descricao = models.TextField(blank=True, null=True)
    territorio = models.ForeignKey(Territorio, models.CASCADE, related_name="conflitos")
    tipo_conflito = models.ForeignKey(TipoConflito, models.PROTECT)
    criacao = models.DateTimeField(auto_now_add=True, editable=False)
    ultima_alteracao = models.DateTimeField(auto_now=True, editable=False)

    class Meta:
        pass


# ORGANIZAÇÃO
class Organizacao(BaseModel):
    nome = models.CharField(max_length=250)
    usuarias = models.ManyToManyField(Usuaria, through='OrganizacaoUsuaria', related_name='organizacoes')
    criacao = models.DateTimeField(auto_now_add=True, editable=False)
    ultima_alteracao = models.DateTimeField(auto_now=True, editable=False)

    def __str__(self):
        return self.nome

    def get_absolute_url(self):
        return reverse('organizacao_detail', args=(self.pk,))

    def get_update_url(self):
        return reverse('organizacao_update', args=(self.pk,))

    class Meta:
        pass


class OrganizacaoUsuaria(models.Model):
    is_admin = models.BooleanField(default=False)
    is_mapeador = models.BooleanField(default=False)
    is_revisor = models.BooleanField(default=False)
    organizacao = models.ForeignKey(Organizacao, models.CASCADE, related_name="org_usuarias")
    usuaria = models.ForeignKey(Usuaria, models.CASCADE, related_name="org_usuarias")

    class Meta:
        pass


# CHAT / MENSAGERIA
class Mensagem(models.Model):
    remetente = models.ForeignKey(Usuaria, models.CASCADE, related_name="mensagens_enviadas", null=True)
    texto = models.TextField(blank=True, null=True)
    extra = JSONField(default=None, blank=True, null=True)
    data_envio = models.DateTimeField(blank=True, null=True)
    data_recebimento = models.DateTimeField(blank=True, null=True)
    origin_is_device = models.BooleanField(default=False)
    territorio = models.ForeignKey(Territorio, models.CASCADE, related_name="mensagens")

    class Meta:
        pass

    def create_mensagem_device_objects(self):
        if self.id and self.territorio:
            mds = []
            devices = Device.objects.filter(usuaria__in=self.territorio.usuarias.all(), active=True).all()
            for device in devices:
                mds.append(MensagemDevice.objects.create(mensagem=self, device=device, status=0))
            return mds
        raise IntegrityError("Mensagem precisa estar gravada e ter território")

    @cached_property
    def status(self):
        status = {
            'erro': False,
            'enviada': False,
            'recebida': False
        }
        for mensagem_device in self.mds.all():
            if mensagem_device.status == 1:
                status['enviada'] = True
            elif mensagem_device.status == 2:
                status['recebida'] = True
            elif mensagem_device.status > 2:
                status['erro'] = True
        return status


class MensagemDevice(models.Model):
    TIPOS_STATUS = (
        (0, 'Não enviado'),
        (1, 'Enviada para o dispositivo'),
        (2, 'Recebida pelo dispositivo'),
        (3, 'Não enviou - Erro no push server (provavelmente formatação/validação)'),
        (4, 'Não enviou - Erro de conexão ou HTTP'),
        (5, 'Erro - Dispositivo de destino não encontrado'),
        (6, 'Erro - Outro tipo de erro de notificação'),
    )

    mensagem = models.ForeignKey(Mensagem, models.CASCADE, related_name="mds")
    device = models.ForeignKey(Device, models.CASCADE, related_name="mds")
    status = models.IntegerField(choices=TIPOS_STATUS, default=0)
    error_data = JSONField(default=None, null=True)
