from crispy_forms.helper import FormHelper
from crispy_forms.layout import Button, Submit
from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django.contrib.gis.forms import GeometryField, OpenLayersWidget, BaseGeometryWidget
from django.urls import reverse

from main.models import *


class BaseModelForm(forms.ModelForm):
    can_delete = False
    delete_confirm_message = ''
    delete_success_url = ''
    delete_url = ''

    def __init__(self, *args, **kwargs):
        super(BaseModelForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-lg-2'
        self.helper.field_class = 'col-lg-8'
        if self.can_delete:
            self.helper.add_input(Button('delete-button', "Apagar", css_class='btn btn-danger',
                                         data_delete_confirm_message=self.delete_confirm_message,
                                         data_success_url=self.delete_success_url,
                                         data_delete_url=self.delete_url))
        self.helper.add_input(Submit('submit', 'Gravar'))


class BaseForm(forms.Form):

    def __init__(self, *args, **kwargs):
        super(BaseForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-lg-2'
        self.helper.field_class = 'col-lg-8'
        self.helper.add_input(Submit('submit', 'Gravar'))


class ModoDeVidaForm(BaseModelForm):
    class Meta:
        model = ModoDeVida
        fields = ['nome']


class TipoProducaoForm(BaseModelForm):
    class Meta:
        model = TipoProducao
        fields = ['nome']


class TipoComunidadeForm(BaseModelForm):
    class Meta:
        model = TipoComunidade
        fields = ['nome', 'descricao']


class UsuariaForm(BaseModelForm):
    class Meta:
        model = Usuaria
        fields = ['username', 'first_name', 'last_name', 'email']


class EstadoForm(BaseModelForm):
    class Meta:
        model = Estado
        fields = ['uf', 'nome']


class MunicipioForm(BaseModelForm):
    class Meta:
        model = Municipio
        fields = ['nome', 'estado']


class TerritorioUsuariaForm(BaseModelForm):
    class Meta:
        model = TerritorioUsuaria
        fields = ['is_editor', 'territorio', 'usuaria']


class TipoAnexoForm(BaseModelForm):
    class Meta:
        model = TipoAnexo
        fields = ['nome', 'mae']


class AnexoForm(BaseModelForm):
    class Meta:
        model = Anexo
        fields = ['nome', 'descricao', 'mimetype', 'file_size', 'territorio', 'tipo_anexo']


class TipoAreaDeUsoForm(BaseModelForm):
    class Meta:
        model = TipoAreaDeUso
        fields = ['nome', 'descricao']


class AreaDeUsoForm(BaseModelForm):
    class Meta:
        model = AreaDeUso
        fields = ['nome', 'posicao', 'descricao', 'territorio', 'tipo_area_de_uso']


class TipoConflitoForm(BaseModelForm):
    class Meta:
        model = TipoConflito
        fields = ['nome', 'descricao']


class ConflitoForm(BaseModelForm):
    class Meta:
        model = Conflito
        fields = ['nome', 'posicao', 'descricao', 'territorio', 'tipo_conflito']


class OrganizacaoForm(BaseModelForm):
    def __init__(self, *args, **kwargs):
        self.can_delete = kwargs.pop('can_delete', False)
        if self.can_delete:
            self.delete_confirm_message = 'Deseja realmente apagar esta organização e todos os elementos que fazem parte dela (agroecossistemas, etc)?'
            self.delete_url = reverse('organizacao_delete', kwargs={'pk': kwargs.get('instance').id})
            self.delete_success_url = reverse('organizacao_list')

        super(OrganizacaoForm, self).__init__(*args, **kwargs)

    class Meta:
        model = Organizacao
        fields = ['nome']


class TerritorioForm(BaseModelForm):
    poligono = GeometryField(srid=4326, required=False, widget=BaseGeometryWidget())

    def __init__(self, *args, **kwargs):
        self.can_delete = kwargs.pop('can_delete', False)
        if self.can_delete:
            self.delete_confirm_message = 'Deseja realmente apagar este território e todos os elementos que fazem parte dele (mensagens, etc)?'
            self.delete_url = reverse('territorio_delete', kwargs={'pk': kwargs.get('instance').id})
            self.delete_success_url = reverse('territorio_list')

        super(TerritorioForm, self).__init__(*args, **kwargs)

    class Meta:
        model = Territorio
        fields = ['nome', 'poligono', 'ano_fundacao', 'qtde_familias', 'status', 'modo_de_vida', 'tipo_producao',
                  'municipio_referencia']


class TerritorioDetailForm(BaseModelForm):

    mensagem = forms.CharField(widget=forms.Textarea, required=False)

    class Meta:
        model = Territorio
        # fields = ['poligono', 'status', 'mensagem']
        fields = ['status', 'mensagem']


class OrganizacaoUsuariaForm(forms.ModelForm):
    class Meta:
        model = OrganizacaoUsuaria
        fields = ['is_admin', 'is_mapeador', 'is_revisor', 'organizacao', 'usuaria']


class UsuariaCreationForm(UserCreationForm, BaseModelForm):
    is_organization_admin = forms.BooleanField(label='Pode administrar esta organização?', required=False)
    is_organization_mapeador = forms.BooleanField(label='Pode fazer mapeamento?', required=False)
    is_organization_revisor = forms.BooleanField(label='Revisor?', required=False)

    class Meta:
        model = Usuaria
        fields = ['username', 'first_name', 'last_name', 'email', 'password1', 'password2', 'telefone',
                  'codigo_uso_celular', 'is_organization_admin', 'is_organization_mapeador', 'is_organization_revisor']
        labels = {
            'username': 'Nome de usuário',
            'first_name': 'Nome',
            'last_name': 'Sobrenome',
            'password1': 'Senha',
            'password2': 'Confirmar Senha',
            'email': 'Endereço de e-mail',
            'telefone': 'Número do tel. celular (com DDD, somente números)',
            'codigo_uso_celular': 'Código (para usuário entrar no sistema)'
        }
        required = {
            'email': True,
            'first_name': True,
            'telefone': True,
            'codigo_uso_celular': True
        }


class MensagemCreationForm(BaseModelForm):
    class Meta:
        model = Mensagem
        fields = ['texto']
        labels = {
            'texto': 'Nova mensagem',
            'extra': 'Extra (JSON)'
        }
        required = {
            'texto': True,
            'extra': False
        }


class UsuariaChangeForm(UserChangeForm, BaseModelForm):
    class Meta:
        model = Usuaria
        fields = ['username', 'first_name', 'last_name', 'email', 'telefone', 'codigo_uso_celular']
        labels = {
            'username': 'Nome de usuário',
            'first_name': 'Nome',
            'last_name': 'Sobrenome',
            'email': 'Endereço de e-mail',
            'telefone': 'Número do tel. celular (com DDD, somente números)',
            'codigo_uso_celular': 'Código (para usuário entrar no sistema)'
        }
        required = {
            'email': True,
            'first_name': True,
            'telefone': True,
            'codigo_uso_celular': True,
            'password1': False,
            'password2': False
        }

    """
    ModoDeVida
    TipoComunidade
    TipoProducao
    Territorio

    Usuaria
    Estado
    Municipio
    TerritorioUsuaria
    TipoAnexo
    Anexo
    TipoAreaDeUso
    AreaDeUso
    TipoConflito
    Conflito
    Organizacao
    OrganizacaoUsuaria
    """
