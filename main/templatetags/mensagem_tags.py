from django import template
from django.templatetags.static import static
from django.conf import settings

register = template.Library()


@register.inclusion_tag('main/inclusiontags/mensagem_status_tag.html')
def mensagem_status(mensagem):
    status = mensagem.status
    icone = "fa-circle"
    css_class = ""
    if status['erro']:
        icone = "fa-exclamation-circle"
    elif status['enviada']:
        icone = "fa-check-circle"

    if status['enviada']:
        css_class="text-success"
    elif status['erro']:
        css_class="text-danger"

    return {'mensagem': mensagem, 'icone': icone, 'css_class': css_class}

