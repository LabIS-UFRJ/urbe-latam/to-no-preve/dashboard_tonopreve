from django.template import Library
import simplejson
import json
import jsonpickle
from json import JSONEncoder

register = Library()


# Usage:
# {{ book.objects.all | jsonify_queryset:"id,title" }}
@register.filter
def jsonify_queryset(object, fields):
    return simplejson.dumps(list(object.values(*fields.split(','))))
