from rest_framework import serializers
from .models import Anexo


class AnexoSerializer(serializers.ModelSerializer):
    thumb = serializers.ImageField(read_only=True)
    class Meta:
        model = Anexo
        fields = ('id', 'nome', 'descricao', 'mimetype', 'file_size', 'arquivo', 'tipo_anexo', 'territorio', 'criacao', 'ultima_alteracao', 'thumb')
