'use strict';

window.chartColors = {
	red: 'rgb(230, 25, 75)',
	green: 'rgb(60, 180, 75)',
	yellow: 'rgb(255, 225, 25)',
	blue: 'rgb(0, 130, 200)',
	orange: 'rgb(245, 130, 48)',
	purple: 'rgb(145, 30, 180)',
	cyan: 'rgb(70, 240, 240)',
	magenta: 'rgb(240, 50, 230)',
	lime: 'rgb(210, 245, 60)',
	pink: 'rgb(250, 190, 190)',
	teal: 'rgb(0, 128, 128)',
	lavender: 'rgb(230, 190, 255)',
	brown: 'rgb(170, 110, 40)',
	beige: 'rgb(255, 250, 200)',
	maroon: 'rgb(128, 0, 0)',
	mint: 'rgb(170, 255, 195)',
	olive: 'rgb(128, 128, 0)',
	apricot: 'rgb(255, 215, 180)',
	navy: 'rgb(0, 0, 128)',
	grey: 'rgb(128, 128, 128)',
	pb: 'rgb(60, 180, 75)',
	estoque: 'rgb(250, 190, 190)',
	doacoes_trocas: 'rgb(255, 225, 25)',
	autoconsumo: 'rgb(0, 130, 200)',
	venda: 'rgb(245, 130, 48)',
	ci: 'rgb(145, 30, 180)',
	va: 'rgb(70, 240, 240)',
	cift: 'rgb(240, 50, 230)',
	vat: 'rgb(210, 245, 60)',
	cp: 'rgb(230, 25, 75)',
	ra: 'rgb(0, 128, 128)',
	ram: 'rgb(230, 190, 255)',
	pt: 'rgb(0, 255, 58)',
	rna_pluriatividade: 'rgb(60, 180, 75)',
	rna_transferencia: 'rgb(255, 225, 25)',
	mercantil: 'rgb(60, 180, 75)',
	domestico_cuidados: 'rgb(145, 30, 180)',
	participacao_social: 'rgb(255, 225, 25)',
	pluriatividade: 'rgb(0, 130, 200)',
	homens: 'rgb(245, 130, 48)',
	mulheres: 'rgb(240, 50, 230)',
	purpless: 'rgb(145, 30, 180)',
	cyanss: 'rgb(70, 240, 240)',
};

window.chartColorsOnly = ['rgb(230, 25, 75)', 'rgb(60, 180, 75)', 'rgb(255, 225, 25)', 'rgb(0, 130, 200)', 'rgb(245, 130, 48)', 'rgb(145, 30, 180)', 'rgb(70, 240, 240)', 'rgb(240, 50, 230)', 'rgb(210, 245, 60)', 'rgb(250, 190, 190)', 'rgb(0, 128, 128)', 'rgb(230, 190, 255)', 'rgb(170, 110, 40)', 'rgb(255, 250, 200)', 'rgb(128, 0, 0)', 'rgb(170, 255, 195)', 'rgb(128, 128, 0)', 'rgb(255, 215, 180)', 'rgb(0, 0, 128)', 'rgb(128, 128, 128)'];

window.coresProdutos = ['rgb(1, 0, 103)', 'rgb(213, 255, 0)', 'rgb(255, 0, 86)', 'rgb(158, 0, 142)', 'rgb(14, 76, 161)', 'rgb(255, 229, 2)', 'rgb(0, 95, 57)', 'rgb(0, 255, 0)', 'rgb(149, 0, 58)', 'rgb(255, 147, 126)', 'rgb(164, 36, 0)', 'rgb(0, 21, 68)', 'rgb(145, 208, 203)', 'rgb(98, 14, 0)', 'rgb(107, 104, 130)', 'rgb(0, 0, 255)', 'rgb(0, 125, 181)', 'rgb(106, 130, 108)', 'rgb(0, 174, 126)', 'rgb(194, 140, 159)', 'rgb(190, 153, 112)', 'rgb(0, 143, 156)', 'rgb(95, 173, 78)', 'rgb(255, 0, 0)', 'rgb(255, 0, 246)', 'rgb(255, 2, 157)', 'rgb(104, 61, 59)', 'rgb(255, 116, 163)', 'rgb(150, 138, 232)', 'rgb(152, 255, 82)', 'rgb(167, 87, 64)', 'rgb(1, 255, 254)', 'rgb(255, 238, 232)', 'rgb(254, 137, 0)', 'rgb(189, 198, 255)', 'rgb(1, 208, 255)', 'rgb(187, 136, 0)', 'rgb(117, 68, 177)', 'rgb(165, 255, 210)', 'rgb(255, 166, 254)', 'rgb(119, 77, 0)', 'rgb(122, 71, 130)', 'rgb(38, 52, 0)', 'rgb(0, 71, 84)', 'rgb(67, 0, 44)', 'rgb(181, 0, 255)', 'rgb(255, 177, 103)', 'rgb(255, 219, 102)', 'rgb(144, 251, 146)', 'rgb(126, 45, 210)', 'rgb(189, 211, 147)', 'rgb(229, 111, 254)', 'rgb(222, 255, 116)', 'rgb(0, 255, 120)', 'rgb(0, 155, 255)', 'rgb(0, 100, 1)', 'rgb(0, 118, 255)', 'rgb(133, 169, 0)', 'rgb(0, 185, 23)', 'rgb(120, 130, 49)', 'rgb(0, 255, 198)', 'rgb(255, 110, 65)', 'rgb(232, 94, 190)', 'rgb(0, 0, 0)'];

(function(global) {
	var Months = [
		'January',
		'February',
		'March',
		'April',
		'May',
		'June',
		'July',
		'August',
		'September',
		'October',
		'November',
		'December'
	];

	var COLORS = [
		'#4dc9f6',
		'#f67019',
		'#f53794',
		'#537bc4',
		'#acc236',
		'#166a8f',
		'#00a950',
		'#58595b',
		'#8549ba'
	];

	var Samples = global.Samples || (global.Samples = {});
	var Color = global.Color;

	Samples.utils = {
		// Adapted from http://indiegamr.com/generate-repeatable-random-numbers-in-js/
		srand: function(seed) {
			this._seed = seed;
		},

		rand: function(min, max) {
			var seed = this._seed;
			min = min === undefined ? 0 : min;
			max = max === undefined ? 1 : max;
			this._seed = (seed * 9301 + 49297) % 233280;
			return min + (this._seed / 233280) * (max - min);
		},

		numbers: function(config) {
			var cfg = config || {};
			var min = cfg.min || 0;
			var max = cfg.max || 1;
			var from = cfg.from || [];
			var count = cfg.count || 8;
			var decimals = cfg.decimals || 8;
			var continuity = cfg.continuity || 1;
			var dfactor = Math.pow(10, decimals) || 0;
			var data = [];
			var i, value;

			for (i = 0; i < count; ++i) {
				value = (from[i] || 0) + this.rand(min, max);
				if (this.rand() <= continuity) {
					data.push(Math.round(dfactor * value) / dfactor);
				} else {
					data.push(null);
				}
			}

			return data;
		},

		labels: function(config) {
			var cfg = config || {};
			var min = cfg.min || 0;
			var max = cfg.max || 100;
			var count = cfg.count || 8;
			var step = (max - min) / count;
			var decimals = cfg.decimals || 8;
			var dfactor = Math.pow(10, decimals) || 0;
			var prefix = cfg.prefix || '';
			var values = [];
			var i;

			for (i = min; i < max; i += step) {
				values.push(prefix + Math.round(dfactor * i) / dfactor);
			}

			return values;
		},

		months: function(config) {
			var cfg = config || {};
			var count = cfg.count || 12;
			var section = cfg.section;
			var values = [];
			var i, value;

			for (i = 0; i < count; ++i) {
				value = Months[Math.ceil(i) % 12];
				values.push(value.substring(0, section));
			}

			return values;
		},

		color: function(index) {
			return COLORS[index % COLORS.length];
		},

		transparentize: function(color, opacity) {
			var alpha = opacity === undefined ? 0.5 : 1 - opacity;
			return Color(color).alpha(alpha).rgbString();
		}
	};

	// DEPRECATED
	window.randomScalingFactor = function() {
		return Math.round(Samples.utils.rand(-100, 100));
	};

	// INITIALIZATION

	Samples.utils.srand(Date.now());

}(this));
