import json

import graphene
from django.db import transaction
from django.db.utils import IntegrityError
from graphene import Field, String, Argument
from graphene.types.scalars import Int
from graphene_django.forms.mutation import DjangoModelFormMutation
from graphene_django.types import DjangoObjectType
from rest_framework_jwt.serializers import JSONWebTokenSerializer
from rest_framework_jwt.settings import api_settings
from graphene_file_upload.scalars import Upload

from main.forms import *
from main import models


class ModoDeVida(DjangoObjectType):
    class Meta:
        model = models.ModoDeVida


class TipoComunidade(DjangoObjectType):
    class Meta:
        model = models.TipoComunidade


class Territorio(DjangoObjectType):
    tipo_comunidade_id = Field(String)
    modo_de_vida_id = Field(String)
    tipo_producao_id = Field(String)
    municipio_referencia_id = Field(String)
    status_id = Field(Int)
    tipos_comunidade = graphene.List(TipoComunidade)

    class Meta:
        model = models.Territorio
        geojson_field = 'poligono'

    def resolve_tipo_comunidade_id(self, info, **kwargs):
        tipo_comunidade = self.tipos_comunidade.first()
        if tipo_comunidade:
            return tipo_comunidade.id
        return None

    def resolve_tipos_comunidade(self, info, **kwargs):
        return self.tipos_comunidade.all()


class SimpleTerritorio(DjangoObjectType):
    municipio_referencia_id = Field(String)

    class Meta:
        model = models.Territorio
        fields = ('id', 'municipio_referencia_id', 'nome')


class TerritorioMutation(DjangoModelFormMutation):
    territorio = Field(Territorio)

    class Meta:
        form_class = TerritorioForm
        return_field_name = "territorio"


class Usuaria(DjangoObjectType):
    class Meta:
        model = models.Usuaria

    current_territorio = Field(Territorio, id=graphene.Int())
    full_name = Field(String)

    def resolve_current_territorio(self, info, **kwargs):
        id_territorio = kwargs.get('id', None)
        return self.current_territorio(id_territorio)

    def resolve_full_name(self, info):
        return self.first_name + ' ' + self.last_name


class TerritorioStatus(DjangoObjectType):
    class Meta:
        model = models.TerritorioStatus


class Estado(DjangoObjectType):
    class Meta:
        model = models.Estado


class Municipio(DjangoObjectType):
    class Meta:
        model = models.Municipio


class TipoProducao(DjangoObjectType):
    class Meta:
        model = models.TipoProducao


class TipoAnexo(DjangoObjectType):
    class Meta:
        model = models.TipoAnexo


class Anexo(DjangoObjectType):
    thumb = Field(String)

    def resolve_thumb(self, info):
        if self.thumb.name:
            return self.thumb.url
        return None

    class Meta:
        model = models.Anexo


class TipoAreaDeUso(DjangoObjectType):
    class Meta:
        model = models.TipoAreaDeUso


class AreaDeUso(DjangoObjectType):
    tipo_area_de_uso_id = Field(String)
    novo_tipo_area_de_uso = Field(TipoAreaDeUso)
    class Meta:
        model = models.AreaDeUso


class TipoConflito(DjangoObjectType):
    class Meta:
        model = models.TipoConflito


class Conflito(DjangoObjectType):
    tipo_conflito_id = Field(String)
    novo_conflito = Field(TipoConflito)

    class Meta:
        model = models.Conflito


class Organizacao(DjangoObjectType):
    class Meta:
        model = models.Organizacao


class OrganizacaoUsuaria(DjangoObjectType):
    class Meta:
        model = models.OrganizacaoUsuaria


class TerritorioUsuaria(DjangoObjectType):
    class Meta:
        model = models.TerritorioUsuaria


class Mensagem(DjangoObjectType):
    class Meta:
        model = models.Mensagem


class Query(graphene.AbstractType):
    current_user = graphene.Field(Usuaria)
    territorios = graphene.List(SimpleTerritorio, id_usuaria=graphene.Int())
    mensagens = graphene.List(Mensagem, since=graphene.DateTime())

    modos_de_vida = graphene.List(ModoDeVida)
    tipos_producao = graphene.List(TipoProducao)
    tipos_comunidade = graphene.List(TipoComunidade)
    tipos_conflito = graphene.List(TipoConflito)
    tipos_area_de_uso = graphene.List(TipoAreaDeUso)
    tipos_anexo = graphene.List(TipoAnexo)
    territorio_status = graphene.List(TerritorioStatus)

    territorio = graphene.Field(Territorio,
                                id=graphene.Int())

    def resolve_modos_de_vida(self, info, **kwargs):
        return models.ModoDeVida.objects.all()

    def resolve_tipos_producao(self, info, **kwargs):
        return models.TipoProducao.objects.all()

    def resolve_tipos_comunidade(self, info, **kwargs):
        return models.TipoComunidade.objects.all()

    def resolve_tipos_conflito(self, info, **kwargs):
        return models.TipoConflito.objects.all()

    def resolve_tipos_area_de_uso(self, info, **kwargs):
        return models.TipoAreaDeUso.objects.all()

    def resolve_tipos_anexo(self, info, **kwargs):
        return models.TipoAnexo.objects.all()

    def resolve_territorio_status(self, info, **kwargs):
        return models.TerritorioStatus.objects.all()

    def resolve_territorios(self, info, **kwargs):
        id_usuaria = kwargs.get('id_usuaria', None)
        if id_usuaria:
            return models.Usuaria.objects.get(pk=id_usuaria).territorios.order_by('nome').all()
        else:
            return models.Territorio.objects.order_by('nome').all()

    def resolve_current_user(self, info, **kwargs):
        context = info.context
        if not context.user.is_authenticated:
            return None
        return context.user

    def resolve_territorio(self, info, **kwargs):
        id = kwargs.get('id')

        if id is not None:
            return models.Territorio.objects.get(pk=id)

        return None

    def resolve_mensagens(self, info, **kwargs):
        return []


# ------------------------------------------------------------ #
# Mutações de usuária

class AnexoInput(graphene.InputObjectType):
    id = graphene.ID(required=False)
    nome = graphene.String()
    descricao = graphene.String()
    mimetype = graphene.String()
    file_size = graphene.Int()
    arquivo = graphene.String(required=False)
    tipo_anexo_id = graphene.String()
    territorio_id = graphene.String()
    criacao = graphene.DateTime()
    ultima_alteracao = graphene.DateTime()
    thumb = graphene.String(required=False)


class MensagemInput(graphene.InputObjectType):
    id = graphene.ID(required=False)
    remetente_id = graphene.String(required=False)
    texto = graphene.String()
    extra = graphene.String(required=False)
    data_envio = graphene.DateTime(required=False)
    data_recebimento = graphene.DateTime(required=False)
    origin_is_device = graphene.Boolean(required=False)
    territorio_id = graphene.String(required=False)


class TipoAreaDeUsoInput(graphene.InputObjectType):
    nome = graphene.String()
    descricao = graphene.String()



class AreaDeUsoInput(graphene.InputObjectType):
    id = graphene.ID(required=False)
    nome = graphene.String()
    posicao = graphene.String()
    descricao = graphene.String()
    tipo_area_de_uso_id = graphene.String()
    novo_tipo_area_de_uso = graphene.InputField(TipoAreaDeUsoInput)


class TipoConflitoInput(graphene.InputObjectType):
    nome = graphene.String()
    descricao = graphene.String()

class ConflitoInput(graphene.InputObjectType):
    id = graphene.ID(required=False)
    nome = graphene.String()
    posicao = graphene.String()
    descricao = graphene.String()
    tipo_conflito_id = graphene.String()
    novo_tipo_conflito = graphene.InputField(TipoConflitoInput)


class TipoComunidadeInput(graphene.InputObjectType):
    id = graphene.ID(required=False)
    nome = graphene.String()
    descricao = graphene.String()


class TerritorioInput(graphene.InputObjectType):
    id = graphene.ID(required=False)
    nome = graphene.String(required=True)
    poligono = graphene.String()
    ano_fundacao = graphene.Int()
    qtde_familias = graphene.Int()
    status_id = graphene.Int()
    modo_de_vida_id = graphene.String()
    tipo_producao_id = graphene.String()
    tipo_comunidade_id = graphene.String()
    municipio_referencia_id = graphene.Int(required=True)
    conflitos = graphene.List(ConflitoInput)
    areas_de_uso = graphene.List(AreaDeUsoInput)
    anexos = graphene.List(AnexoInput)
    mensagens = graphene.List(MensagemInput)
    publico = graphene.Boolean(required=False, default_value=False)
    tipos_comunidade = graphene.List(TipoComunidadeInput)
    anexo_ata = graphene.InputField(AnexoInput)

    def get_local_data(self):
        # tirar os campos foreign_key para inserir/atualizar territorio
        local_data = dict(self.__dict__)
        local_data.pop("conflitos", None)
        local_data.pop("areas_de_uso", None)
        local_data.pop("anexos", None)
        local_data.pop("mensagens", None)
        local_data.pop("tipos_comunidade", None)
        local_data.pop("anexo_ata", None)
        local_data.pop("tipo_comunidade_id", None)
        return local_data


class OrganizacaoInput(graphene.InputObjectType):
    id = graphene.ID(required=True)
    nome = graphene.String(required=False)


class UsuariaInput(graphene.InputObjectType):
    id = graphene.ID(required=False)
    username = graphene.String(required=True)
    full_name = graphene.String()
    current_territorio = graphene.InputField(TerritorioInput)
    organizacoes = graphene.List(OrganizacaoInput)
    push_token = graphene.String()
    cpf = graphene.String()


class DataSourceInput(graphene.InputObjectType):
    current_user = graphene.InputField(UsuariaInput)


def inserir_territorio(territorio_input, context):
    with transaction.atomic():
        territorio_data = territorio_input.get_local_data()

        territorio, territorio_created = models.Territorio.objects.update_or_create(pk=territorio_input.id,
                                                                                    defaults=territorio_data)

        if territorio_input.conflitos is not None:
            novos_conflitos_id = []
            for conflito_input in territorio_input.conflitos:
                conflito_data = dict(conflito_input.__dict__)
                conflito_data["territorio_id"] = territorio.id
                teste = conflito_data["novo_tipo_conflito"]
                if(teste):
                    tc = models.TipoConflito(nome=teste.nome,descricao=teste.descricao)
                    tc.save()
                    conflito_data["tipo_conflito_id"] = tc.id
                conflito_data.pop('novo_tipo_conflito', None)
                conflito, created = models.Conflito.objects.update_or_create(pk=conflito_input.id,
                                                                             defaults=conflito_data)
                novos_conflitos_id.append(conflito.id)
            models.Conflito.objects.filter(territorio_id=territorio.id).exclude(id__in=novos_conflitos_id).delete()

        if territorio_input.areas_de_uso is not None:
            novas_areas_de_uso_id = []
            for area_de_uso_input in territorio_input.areas_de_uso:
                area_de_uso_data = dict(area_de_uso_input.__dict__)
                area_de_uso_data["territorio_id"] = territorio.id
                teste = area_de_uso_data["novo_tipo_area_de_uso"]
                if(teste):
                    tau = models.TipoAreaDeUso(nome=teste.nome,descricao=teste.descricao)
                    tau.save()
                    area_de_uso_data["tipo_area_de_uso_id"] = tau.id
                area_de_uso_data.pop('novo_tipo_area_de_uso', None)
                area_de_uso, created = models.AreaDeUso.objects.update_or_create(pk=area_de_uso_input.id,
                                                                                 defaults=area_de_uso_data)
                novas_areas_de_uso_id.append(area_de_uso.id)
            models.AreaDeUso.objects.filter(territorio_id=territorio.id).exclude(id__in=novas_areas_de_uso_id).delete()
        if territorio_input.anexos is not None:
            novos_anexos_id = []
            for anexo_input in territorio_input.anexos:
                anexo_data = dict(anexo_input.__dict__)
                if anexo_data["id"] is not None:
                    try:
                        anexo = models.Anexo.objects.get(pk=anexo_input.id)
                    except models.Anexo.DoesNotExist:
                        continue
                    if int(anexo.territorio_id) != int(territorio.id):
                        continue
                    anexo.nome = anexo_input.nome
                    anexo.descricao = anexo_input.descricao
                    anexo.save()
                    novos_anexos_id.append(anexo.id)
            models.Anexo.objects.filter(territorio_id=territorio.id).exclude(id__in=novos_anexos_id).delete()
        if territorio_input.mensagens is not None:
            for mensagem_input in territorio_input.mensagens:
                if mensagem_input.id is None:
                    mensagem = models.Mensagem(
                        remetente=context.user,
                        territorio=territorio,
                        texto=mensagem_input.texto,
                        extra=None,
                        data_envio=timezone.now(),
                        origin_is_device=True
                    )
                    mensagem.save()
                    break
        if territorio_input.tipos_comunidade is not None:
            tipos_comunidade = []
            for tipo_comunidade_input in territorio_input.tipos_comunidade:
                tipo_comunidade_data = dict(tipo_comunidade_input.__dict__)
                try:
                    tipo_comunidade = models.TipoComunidade.objects.get(pk=tipo_comunidade_data["id"])
                except models.TipoComunidade.DoesNotExist:
                    continue
                tipos_comunidade.append(tipo_comunidade)
            territorio.tipos_comunidade.clear()
            territorio.tipos_comunidade.add(*tipos_comunidade)
            territorio.save()
        if territorio_input.tipo_comunidade_id is not None:
            try:
                tipo_comunidade = models.TipoComunidade.objects.get(pk=territorio_input.tipo_comunidade_id)
                territorio.tipos_comunidade.clear()
                territorio.tipos_comunidade.add(tipo_comunidade)
                territorio.save()
            except models.TipoComunidade.DoesNotExist:
                pass
        if territorio_input.anexo_ata is not None:
            anexo_data = dict(territorio_input.anexo_ata.__dict__)
            if anexo_data["id"] is not None:
                try:
                    anexo = models.Anexo.objects.get(pk=territorio_input.anexo_ata.id)
                    anexo.nome = territorio_input.anexo_ata.nome
                    anexo.descricao = territorio_input.anexo_ata.descricao
                    anexo.save()
                    territorio.anexo_ata = anexo
                    territorio.save()
                except models.Anexo.DoesNotExist:
                    pass
        return [territorio, territorio_created]


class UpdateAppData(graphene.Mutation):
    class Arguments:
        input = Argument(DataSourceInput)

    token = graphene.String()
    success = graphene.Boolean()
    errors = graphene.List(graphene.String)
    current_user = graphene.Field(lambda: Usuaria)

    def mutate(self, info, input=None):
        with transaction.atomic():
            usuaria = None
            token = None
            errors = []
            success = False
            context = info.context

            if input.current_user is not None:
                if input.current_user.id is not None:
                    # verificar se pessoa tentando inserir é a mesma do current_user
                    if context.user.is_authenticated and int(context.user.id) == int(input.current_user.id):
                        # atualiza o nome (e futuramente outras propriedades) de usuária, se mudou
                        user_change = False
                        if input.current_user.full_name != context.user.first_name + ' ' + context.user.last_name:
                            context.user.set_full_name(input.current_user.full_name)
                            user_change = True
                        if input.current_user.cpf is not None and input.current_user.cpf != context.user.cpf:
                            context.user.cpf = input.current_user.cpf
                            user_change = True
                        if user_change:
                            context.user.save()
                        usuaria = context.user
                        usuaria.create_or_update_device(input.current_user.push_token)

                else:
                    # insere nova usuária
                    try:
                        # gera senha temporária
                        senha_aleatoria = models.Usuaria.gera_senha_aleatoria()

                        full_name_splitted = re.sub(" +", " ", input.current_user.full_name).strip().split(" ", 1)
                        if len(full_name_splitted) == 1:
                            first_name = full_name_splitted[0]
                            last_name = ''
                        else:
                            first_name, last_name = full_name_splitted

                        usuaria = models.Usuaria.objects.create_user(
                            username=input.current_user.username,
                            password=senha_aleatoria,
                            first_name=first_name,
                            last_name=last_name,
                            codigo_uso_celular=senha_aleatoria,
                            is_active=True,
                            cpf=input.current_user.cpf
                        )
                        # generate token for user
                        jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
                        jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

                        payload = jwt_payload_handler(usuaria)
                        token = jwt_encode_handler(payload)

                        usuaria.create_or_update_device(input.current_user.push_token)

                        # serializer = JSONWebTokenSerializer(
                        #     data={'username': input.current_user.username, 'password': senha_aleatoria})
                        # if serializer.is_valid():
                        #     token = serializer.object["token"]
                        # else:
                        #     i = 1
                    except IntegrityError as exp:
                        errors = ["username", "Telefone já cadastrado"]

            if usuaria is not None and input.current_user.current_territorio is not None:
                territorio, created = inserir_territorio(input.current_user.current_territorio, context)
                if created:
                    models.TerritorioUsuaria.objects.create(territorio_id=territorio.id, usuaria_id=usuaria.id)

            if len(errors) == 0 and usuaria:
                success = True

            return UpdateAppData(token=token, errors=errors, success=success, current_user=usuaria)


# ------------------------------------------------------------ #

class Mutation(graphene.ObjectType):
    update_territorio = TerritorioMutation.Field()
    update_app_data = UpdateAppData.Field()
