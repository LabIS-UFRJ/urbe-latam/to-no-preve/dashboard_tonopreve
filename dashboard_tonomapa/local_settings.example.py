# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '*changeme*changeme*changeme*changeme*changeme*chan'

DEBUG = True

DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'NAME': 'dbname',
        'USER': 'dbuser',
        # 'PASSWORD': '',
        # 'HOST': '127.0.0.1',
        # 'PORT': '5432',
    }
}
